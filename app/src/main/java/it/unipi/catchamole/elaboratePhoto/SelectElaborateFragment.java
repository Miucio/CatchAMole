package it.unipi.catchamole.elaboratePhoto;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.IOException;

import it.unipi.catchamole.R;

/**
 * Fragment of ElaborateActivity that show the image selected and it's possible elaborate
 * the image or return on home
 */

public class SelectElaborateFragment extends Fragment {

    private ImageView imageView;
    private ProgressBar pgsBar;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String FOLDER_PATH = "folderPath";
    private static final String FOLDER_NAME = "folderName";
    private static final String IMAGE_NAME = "imageName";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pgsBar = (ProgressBar) view.findViewById(R.id.circularProgressInd);

        //set image
        imageView = view.findViewById(R.id.showImageView);
        setImageView();

        Button elborateButton = view.findViewById(R.id.selectImageButton);
        elborateButton.setOnClickListener(view2 -> {
            try {
                elaborateImage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Button deleteButton = view.findViewById(R.id.deleteImageButton);
        deleteButton.setOnClickListener(view2 -> ((ElaborateActivity) getActivity()).
                deleteImage(this));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_elaborate, container, false);
    }

    private void setImageView() {
        imageView.setImageBitmap(((ElaborateActivity) getActivity()).getBitMapFromImagePath());
    }

    /**
     * Set image view on invisible and set the progress bar to visible
     * After that will call elaboratePhoto to elaborate the image
     * In conclusion open the SelectElaborateFragment
     */
    protected void elaborateImage() throws IOException {
        imageView.setVisibility(View.INVISIBLE);
        pgsBar.setVisibility(View.VISIBLE);

        ((ElaborateActivity) getActivity()).elaboratePhoto();

        NavHostFragment.findNavController(SelectElaborateFragment.this)
                .navigate(R.id.action_Select_to_Result);
    }
}