package it.unipi.catchamole.elaboratePhoto;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.unipi.catchamole.R;
import it.unipi.catchamole.home.BodyActivity;
import it.unipi.catchamole.image.ImageActivity;
import it.unipi.catchamole.store.CollectionType;
import it.unipi.catchamole.store.SharedStore;
import it.unipi.catchamole.utils.BodyPartsType;
import it.unipi.catchamole.utils.ClassifiedImage;
import it.unipi.catchamole.utils.MoleClassifier;

/**
 * Activity to elaborate the photo and show the results
 * It has 2 fragments, ResultElaborateFragment and SelectElaborateFragment
 */
public class ElaborateActivity extends AppCompatActivity {

    private static final String FOLDER_PATH = "folderPath";
    private static final String FOLDER_NAME = "folderName";
    private static final String RESULTS = "results";
    private static final String IMAGE_NAME = "imageName";
    private static final String EXTRA_MESSAGE = "bodyPart";

    private Map<String, Object> params = new HashMap();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent imageIntent = getIntent();
        params.put(FOLDER_PATH, imageIntent.getStringExtra(FOLDER_PATH));
        params.put(FOLDER_NAME, imageIntent.getStringExtra(FOLDER_NAME));
        params.put(IMAGE_NAME, imageIntent.getStringExtra(IMAGE_NAME));
        params.put(EXTRA_MESSAGE, imageIntent.getStringExtra(EXTRA_MESSAGE));

        setContentView(R.layout.activity_elaborate);
    }

    /**
     * Go on Main activity
     */
    protected void returnMainActivity() {
        Intent cancelIntent = new Intent(this, BodyActivity.class);
        startActivity(cancelIntent);
    }

    /**
     * Go on Gallery
     */
    protected void returnImageActiviy() {
        Intent deleteImageIntent = new Intent(this, ImageActivity.class);
        deleteImageIntent.putExtra(EXTRA_MESSAGE, (String) params.get(EXTRA_MESSAGE));
        startActivity(deleteImageIntent);
    }

    /**
     * Delete the image and result
     */
    protected void deleteImage(Fragment selectFragment) {
        SharedStore sharedStore = SharedStore.getInstance();
        List<ClassifiedImage> classifiedImages = sharedStore.getValues(CollectionType.CLASSIFICATIONS,
                this, ClassifiedImage.class);
        List<ClassifiedImage> removes = new ArrayList<>();

        for (ClassifiedImage classifiedImage : classifiedImages) {
            if (classifiedImage.getFileName().equals(params.get(IMAGE_NAME))) {
                removes.add(classifiedImage);
            }
        }
        classifiedImages.removeAll(removes);
        sharedStore.saveValues(classifiedImages, CollectionType.CLASSIFICATIONS, this,
                ClassifiedImage.class);
        try {
            File f = new File((String) params.get(FOLDER_PATH), (String) params.get(IMAGE_NAME));
            f.delete();
        } catch (Exception e) {
            Log.e("Error file", e.getMessage());
        }

        returnImageActiviy();
    }

    /**
     * Elaborate the photo and it will return the string with the results
     */

    protected boolean elaboratePhoto() throws IOException {
        MoleClassifier moleClassifier = new MoleClassifier(getApplicationContext());

        float classification = moleClassifier.classify(getBitMapFromImagePath());

        saveElaboration(classification);

        params.put(RESULTS, classification);
        return true;
    }

    private ClassifiedImage saveElaboration(float elaboration) {
        ClassifiedImage elaborationImage = new ClassifiedImage((String) params.get(FOLDER_PATH),
                (String) params.get(FOLDER_NAME), (String) params.get(IMAGE_NAME),
                elaboration, BodyPartsType.fromString((String) params.get(FOLDER_NAME)));

        SharedStore sharedStore = SharedStore.getInstance();
        sharedStore.addValue(elaborationImage, CollectionType.CLASSIFICATIONS,
                this, ClassifiedImage.class);

        return elaborationImage;
    }

    public Bitmap getBitMapFromImagePath() {
        File image = new File((String) params.get(FOLDER_PATH), (String) params.get(IMAGE_NAME));
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
        return bitmap;
    }

    @Override
    public void onBackPressed() {
        //Called when back pressed
        returnMainActivity();
    }

    protected Object getParams(String paramName) {
        return params.get(paramName);
    }

}