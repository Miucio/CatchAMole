package it.unipi.catchamole.elaboratePhoto;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

import it.unipi.catchamole.R;

/**
 * Fragment of ElaborateActivity that show the result and with 2 button
 * it's possible chose the next operation
 */
public class ResultElaborateFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String FOLDER_PATH = "folderPath";
    private static final String FOLDER_NAME = "folderName";
    private static final String RESULTS = "results";

    private static final float OK = 0.30f;
    private static final float MEDIUM = 0.70f;
    private static final float TOTAL = 1f;

    private String folderPath, folderName;
    private float results;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        folderPath = (String) ((ElaborateActivity) getActivity()).getParams(FOLDER_PATH);
        folderName = (String) ((ElaborateActivity) getActivity()).getParams(FOLDER_NAME);
        results = (float) ((ElaborateActivity) getActivity()).getParams(RESULTS);

        TextView percentageView = view.findViewById(R.id.percentageElaboration);
        TextView textView = view.findViewById(R.id.resultsElaborate);
        setText(percentageView, textView, results);

        Button elaborateButton = view.findViewById(R.id.returnHome);
        elaborateButton.setOnClickListener(view2 ->
                ((ElaborateActivity) getActivity()).returnMainActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_result_elaborate, container, false);
    }

    private void setText(TextView numberText, TextView text, float results) {
        numberText.setText(String.format(Locale.getDefault(), "%.4f%%", results*100));

        if (results < OK) {
            numberText.setTextColor(ResourcesCompat.getColor(getResources(), R.color.okGreen, null));
            text.setText(R.string.no_cancer);
        } else if (results < MEDIUM) {
            numberText.setTextColor(ResourcesCompat.getColor(getResources(), R.color.mediumYellow, null));
            text.setText(R.string.medium_cancer);
        } else if ((TOTAL > results) && (results > MEDIUM)) {
            numberText.setTextColor(ResourcesCompat.getColor(getResources(), R.color.highOrange, null));
            text.setText(R.string.high_cancer);
        } else if (results == TOTAL) {
            numberText.setTextColor(ResourcesCompat.getColor(getResources(), R.color.dangerousRed, null));
            text.setText(R.string.yes_cancer);
        } else {
            Log.e("Error float parameter", "Error: result is over the border");
        }

    }


}