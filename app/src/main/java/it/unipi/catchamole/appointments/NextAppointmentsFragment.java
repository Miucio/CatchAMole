package it.unipi.catchamole.appointments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import it.unipi.catchamole.R;
import it.unipi.catchamole.appointments.notifications.NotificationAlarmManager;
import it.unipi.catchamole.store.SharedStore;
import it.unipi.catchamole.store.CollectionType;


/**
 * Enumerator representing internally the type of dialog shown to the user when
 * she clicks on an appointment in the list. The dialog can be used to add
 * an appointment (DialogType.ADD) or to modify an existing one (DialogType.MODIFY).
 */
enum DialogType {
    ADD, MODIFY
}


/**
 * Class representing the fragment showing the next appointments. The fragment is
 * shown inside the ViewPager of AppointmentsActivity.
 */
public class NextAppointmentsFragment extends Fragment implements OnClickListener, OnItemClickListener {
    // Variables referring to the list of appointments visible to the user.
    private ListView nextAppointmentsListView;
    private ArrayAdapter<Appointment> appointmentsAdapter;
    private List<Appointment> nextAppointments;

    /*
     * Variables referring to the dialog used to add/modify/delete an appointment.
     * If the dialog is not visible, then the variables are null.
     * onItemClickPosition and onItemClickId are used to restore correctly
     * a dialog of type DialogType.MODIFY when calling restorePreviousDialog().
     */
    private AlertDialog alertDialog;
    private DialogType alertDialogType;
    private Integer onItemClickPosition;
    private Long onItemClickId;

    // Notification manager.
    private NotificationAlarmManager notificationManager;

    // Collection of appointments.
    private SharedStore appointmentStore;

    /*
     * Flag signaling if moveExpiredAppointments() must be performed or not.
     * The flag must be set to true only when a dialog of type DialogType.MODIFY is being restored
     * with restorePreviousDialog(), to avoid incorrect modifications of the wrong appointment.
     * The flag must be set to false after the dialog is dismissed to restore the normal behaviour.
     *
     * Example of a wrong execution if the flag is not set appropriately:
     * 1) the user selects an appointment X in the list in order to modify it;
     * 2) the dialog to modify the appointment X is shown;
     * 3) the user rotates the device;
     * 4) the activity is recreated and the dialog referring to X is restored.
     *    Since the activity is recreated, also onResume() is called, which calls
     *    in turn moveExpiredAppointments();
     * 5) suppose X is marked as expired during moveExpiredAppointments(). Then, it is
     *    removed from the list of appointments, but the dialog to modify it is still shown
     *    to the user. If the user applies some modifications and saves them, they are applied
     *    to the appointment in the list that took the place of X, which could also lead to
     *    an out of bound exception.
     * Instead, if moveExpiredAppointments() is blocked, the restored dialog still refers
     * to the correct appointment. When the user will conclude the modification, the flag
     * will be set to false and, since onResume() is called afterwards, the appointment can
     * be safely moved to the past appointments list.
     */
    private boolean BLOCK_MOVE_EXPIRED_APPOINTMENTS = false;


    /**
     * Hides to the user the placeholders used when no next appointments are available.
     * This is achieved by making invisible the placeholders defined in the layout.
     */
    private void hideNoAppointments() {
        ImageView placeholderImage = getView().findViewById(R.id.no_next_appointments_image);
        TextView placeholderText = getView().findViewById(R.id.no_next_appointments_text);

        placeholderImage.setVisibility(View.INVISIBLE);
        placeholderText.setVisibility(View.INVISIBLE);
    }

    /**
     * Shows to the user the placeholders used when no next appointments are available.
     * This is achieved by making visible the placeholders defined in the layout.
     */
    private void showNoAppointments() {
        ImageView placeholderImage = getView().findViewById(R.id.no_next_appointments_image);
        TextView placeholderText = getView().findViewById(R.id.no_next_appointments_text);

        placeholderImage.setVisibility(View.VISIBLE);
        placeholderText.setVisibility(View.VISIBLE);
    }

    /**
     * Deletes an appointment inside the list of next appointments.
     * @param position  the position of the appointment inside the nextAppointments list.
     */
    private void deleteAppointment(int position) {
        notificationManager.removeNotificationAlarms(nextAppointments.get(position), getActivity());
        nextAppointments.remove(position);
        appointmentStore.saveValues(nextAppointments, CollectionType.NEXT_APPOINTMENTS, getActivity(), Appointment.class);
    }

    /**
     * Updates an appointment inside the list of next appointments.
     * @param position            the position of the appointment inside the nextAppointments list.
     * @param updatedAppointment  the appointment with the updated fields.
     */
    private void updateAppointment(int position, Appointment updatedAppointment) {
        notificationManager.removeNotificationAlarms(nextAppointments.get(position), getActivity());
        notificationManager.createNotificationAlarm(updatedAppointment, getActivity());

        nextAppointments.remove(position);
        nextAppointments.add(updatedAppointment);
        Collections.sort(nextAppointments);

        appointmentStore.saveValues(nextAppointments, CollectionType.NEXT_APPOINTMENTS, getActivity(), Appointment.class);
    }

    /**
     * Saves a new appointment inside the list of next appointments.
     * @param newAppointment  the appointment to save.
     */
    private void saveNewAppointment(Appointment newAppointment) {
        notificationManager.createNotificationAlarm(newAppointment, getActivity());

        nextAppointments.add(newAppointment);
        Collections.sort(nextAppointments);

        appointmentStore.saveValues(nextAppointments, CollectionType.NEXT_APPOINTMENTS, getActivity(), Appointment.class);
    }

    /**
     * Checks if one or more appointments in the list of next appointments have expired
     * and eventually moves them to the list of past appointments.
     */
    private void moveExpiredAppointments() {
        if (BLOCK_MOVE_EXPIRED_APPOINTMENTS)
            return;

        List<Appointment> pastAppointments = appointmentStore.getValues(
                CollectionType.PAST_APPOINTMENTS, getActivity(), Appointment.class);
        long currentTime = Calendar.getInstance().getTimeInMillis();
        boolean updateAppointments = false;

        Iterator<Appointment> iterator = nextAppointments.iterator();
        while (iterator.hasNext()) {
                Appointment appointment = iterator.next(); // Must be called before remove().

            if (appointment.getDate() < currentTime) {
                updateAppointments = true;
                pastAppointments.add(appointment);
                iterator.remove();
            }
        }

        if (updateAppointments) {
            pastAppointments.sort(Collections.reverseOrder());
            appointmentStore.saveValues(nextAppointments, CollectionType.NEXT_APPOINTMENTS, getActivity(), Appointment.class);
            appointmentStore.saveValues(pastAppointments, CollectionType.PAST_APPOINTMENTS, getActivity(), Appointment.class);

            /*
             * Notify PastAppointmentsFragment that one or more appointments
             * have expired and were moved to the past appointments list.
             * The bundle is ignored by PastAppointmentsFragment, so it is left empty.
             */
            getParentFragmentManager().setFragmentResult("appointmentExpired", new Bundle());
        }
    }

    /**
     * Creates a dialog to be shown to the user, assigning its reference
     * to the associated attribute.
     */
    private void createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ViewGroup viewGroup = getView().findViewById(android.R.id.content);

        if (alertDialogType.equals(DialogType.ADD))
            builder.setView(LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_add_appointment, viewGroup, false));
        else if (alertDialogType.equals(DialogType.MODIFY))
            builder.setView(LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_modify_appointment, viewGroup, false));
        else
            return;

        alertDialog = builder.create();
        alertDialog.setOnDismissListener(dialog -> releaseDialogVariables());
    }

    /**
     * Parses the fields of the dialog to obtain the corresponding appointment.
     * If mandatory fields are missing, an error is shown to the user.
     * @param allowEmptyDoctorName  allows to return an appointment with an empty doctor name.
     *                              It must be set to true only when the dialog is saved with
     *                              onSaveInstanceState().
     * @return                      null if the mandatory fields are missing or there is not
     *                              a visible dialog, the parsed appointment otherwise.
     */
    private Appointment parseAppointmentFromDialog(boolean allowEmptyDoctorName) {
        EditText doctorName, otherNotes;
        DatePicker date;
        TimePicker time;

        if (alertDialog == null)
            return null;

        if (alertDialogType.equals(DialogType.ADD)) {
            doctorName = alertDialog.findViewById(R.id.add_appointment_doctor);
            otherNotes = alertDialog.findViewById(R.id.add_appointment_notes);
            date = alertDialog.findViewById(R.id.add_appointment_datepicker);
            time = alertDialog.findViewById(R.id.add_appointment_timepicker);
        } else if (alertDialogType.equals(DialogType.MODIFY)) {
            doctorName = alertDialog.findViewById(R.id.modify_appointment_doctor);
            otherNotes = alertDialog.findViewById(R.id.modify_appointment_notes);
            date = alertDialog.findViewById(R.id.modify_appointment_datepicker);
            time = alertDialog.findViewById(R.id.modify_appointment_timepicker);
        } else
            return null;

        Calendar calendar = Calendar.getInstance();
        calendar.set(date.getYear(), date.getMonth(), date.getDayOfMonth(),
                time.getHour(), time.getMinute());

        if (!allowEmptyDoctorName && TextUtils.isEmpty(doctorName.getText().toString())) {
            doctorName.setError(getActivity().getString(R.string.appointment_dialog_missing_doctor));
            return null;
        }

        return new Appointment(
                doctorName.getText().toString(),
                calendar.getTimeInMillis(),
                otherNotes.getText().toString());
    }

    /**
     * Loads an appointment into the dialog, initializing correctly the relative fields.
     * @param appointment  the appointment to load.
     */
    private void loadAppointmentIntoDialog(Appointment appointment) {
        EditText doctorName, otherNotes;
        DatePicker date;
        TimePicker time;

        if (alertDialog == null || appointment == null)
            return;

        if (alertDialogType.equals(DialogType.ADD)) {
            doctorName = alertDialog.findViewById(R.id.add_appointment_doctor);
            otherNotes = alertDialog.findViewById(R.id.add_appointment_notes);
            date = alertDialog.findViewById(R.id.add_appointment_datepicker);
            time = alertDialog.findViewById(R.id.add_appointment_timepicker);
        } else if (alertDialogType.equals(DialogType.MODIFY)) {
            doctorName = alertDialog.findViewById(R.id.modify_appointment_doctor);
            otherNotes = alertDialog.findViewById(R.id.modify_appointment_notes);
            date = alertDialog.findViewById(R.id.modify_appointment_datepicker);
            time = alertDialog.findViewById(R.id.modify_appointment_timepicker);
        } else
            return;

        doctorName.setText(appointment.getDoctorName());
        otherNotes.setText(appointment.getOtherNotes());

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(appointment.getDate());

        date.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        time.setHour(calendar.get(Calendar.HOUR_OF_DAY));
        time.setMinute(calendar.get(Calendar.MINUTE));
    }

    /**
     * Releases the variables referencing the dialog by setting to null
     * the associated attributes.
     */
    private void releaseDialogVariables() {
        alertDialog = null;
        alertDialogType = null;
        onItemClickPosition = null;
        onItemClickId = null;
    }

    /**
     * Restores a dialog saved with onSaveInstanceState(), if the latter was
     * actually called and the dialog was visible.
     * @param savedInstanceState  the bundle containing the saved instance variables.
     */
    private void restorePreviousDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null || !savedInstanceState.getBoolean("dialogVisible"))
            return;

        Gson gson = new Gson();
        Appointment appointment;

        try {
            alertDialogType = gson.fromJson(
                    savedInstanceState.getString("dialogType"),
                    new TypeToken<DialogType>(){}.getType());

            appointment = gson.fromJson(
                    savedInstanceState.getString("dialogAppointment"),
                    new TypeToken<Appointment>(){}.getType());

        } catch (JsonParseException e) {
            Log.e(this.getClass().getCanonicalName(), e.getMessage());
            return;
        }

        if (alertDialogType.equals(DialogType.ADD))
            onClick(getView());
        else if (alertDialogType.equals(DialogType.MODIFY)) {
            // Block moveExpiredAppointments().
            BLOCK_MOVE_EXPIRED_APPOINTMENTS = true;

            onItemClick(
                    nextAppointmentsListView,
                    getView(),
                    savedInstanceState.getInt("dialogOnItemClickPosition"),
                    savedInstanceState.getLong("dialogOnItemClickId"));
        }

        loadAppointmentIntoDialog(appointment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Get instance of the appointment store.
        appointmentStore = SharedStore.getInstance();

        // Instantiate the notification manager.
        notificationManager = NotificationAlarmManager.getInstance();

        // Inflate the layout.
        return inflater.inflate(R.layout.fragment_next_appointments, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        nextAppointments = appointmentStore.getValues(
                CollectionType.NEXT_APPOINTMENTS, getActivity(), Appointment.class);

        appointmentsAdapter = new ArrayAdapter<>(
                getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_1,
                nextAppointments);

        nextAppointmentsListView = view.findViewById(R.id.next_appointments_list);
        nextAppointmentsListView.setAdapter(appointmentsAdapter);

        // Add a listener to expand each item of the list.
        nextAppointmentsListView.setOnItemClickListener(this);

        // Add a listener to allow the insertion of new appointments.
        FloatingActionButton addButton = view.findViewById(R.id.add_new_appointment);
        addButton.setOnClickListener(this);

        /*
         * If onSaveInstanceState() has been called previously and
         * a dialog was visible, restore it and its information.
         */
        restorePreviousDialog(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        moveExpiredAppointments();

        // Refresh the list every time the fragment is made visible.
        appointmentsAdapter.notifyDataSetChanged();

        if (nextAppointments.isEmpty())
            showNoAppointments();
        else
            hideNoAppointments();
    }

    @Override
    public void onClick(View view) {
        // Create a dialog to add an appointment.
        alertDialogType = DialogType.ADD;
        createDialog();
        alertDialog.show();

        Button confirmButton = alertDialog.findViewById(R.id.add_appointment_confirm_button);
        Button backButton = alertDialog.findViewById(R.id.add_appointment_back_button);

        // Add a listener to save a new appointment.
        confirmButton.setOnClickListener((v) -> {
            Appointment newAppointment = parseAppointmentFromDialog(false);

            if (newAppointment == null)
                return;

            saveNewAppointment(newAppointment);
            Toast.makeText(getActivity(),
                    getActivity().getString(R.string.add_appointment_success_toast),
                    Toast.LENGTH_SHORT).show();

            alertDialog.dismiss();

            // Update the list of appointments shown to the user.
            onResume();
        });

        // Add a listener to close the dialog.
        backButton.setOnClickListener((v) -> alertDialog.dismiss());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Create a dialog to see, modify and delete an appointment.
        onItemClickPosition = position;
        onItemClickId = id;
        alertDialogType = DialogType.MODIFY;

        createDialog();
        alertDialog.show();
        loadAppointmentIntoDialog(nextAppointments.get(position));

        Button confirmButton = alertDialog.findViewById(R.id.modify_appointment_confirm_button);
        Button backButton = alertDialog.findViewById(R.id.modify_appointment_back_button);
        ImageButton deleteButton = alertDialog.findViewById(R.id.modify_appointment_delete_button);

        // Add a listener to save the modified appointment.
        confirmButton.setOnClickListener((v) -> {
            Appointment updatedAppointment = parseAppointmentFromDialog(false);

            if (updatedAppointment == null)
                return;

            updateAppointment(position, updatedAppointment);
            Toast.makeText(getActivity(),
                    getActivity().getString(R.string.modify_appointment_modification_success_toast),
                    Toast.LENGTH_SHORT).show();

            alertDialog.dismiss();

            // Update the list of appointments shown to the user.
            onResume();

            // Unlock moveExpiredAppointments().
            BLOCK_MOVE_EXPIRED_APPOINTMENTS = false;
        });

        // Add a listener to delete the appointment.
        deleteButton.setOnClickListener((v) -> {
            deleteAppointment(position);
            Toast.makeText(getActivity(),
                    getActivity().getString(R.string.modify_appointment_deletion_success_toast),
                    Toast.LENGTH_SHORT).show();

            alertDialog.dismiss();

            // Update the list of appointments shown to the user.
            onResume();

            // Unlock moveExpiredAppointments().
            BLOCK_MOVE_EXPIRED_APPOINTMENTS = false;
        });

        // Add a listener to close the dialog.
        backButton.setOnClickListener((v) -> {
            alertDialog.dismiss();

            // Unlock moveExpiredAppointments().
            BLOCK_MOVE_EXPIRED_APPOINTMENTS = false;
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (alertDialog != null) {
            outState.putBoolean("dialogVisible", true);
            outState.putString("dialogAppointment", new Gson().toJson(parseAppointmentFromDialog(true)));
            outState.putString("dialogType", new Gson().toJson(alertDialogType));

            if (alertDialogType.equals(DialogType.MODIFY)) {
                outState.putInt("dialogOnItemClickPosition", onItemClickPosition);
                outState.putLong("dialogOnItemClickId", onItemClickId);
            }
        } else
            outState.putBoolean("dialogVisible", false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (alertDialog != null)
            alertDialog.dismiss();
    }
}