package it.unipi.catchamole.appointments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.List;

import it.unipi.catchamole.R;
import it.unipi.catchamole.store.SharedStore;
import it.unipi.catchamole.store.CollectionType;


/**
 * Class representing the fragment showing the past appointments. The fragment is
 * shown inside the ViewPager of AppointmentsActivity.
 */
public class PastAppointmentsFragment extends Fragment implements OnItemClickListener {
    // Variables referring to the list of appointments visible to the user.
    private ListView pastAppointmentsListView;
    private ArrayAdapter<Appointment> appointmentsAdapter;
    private List<Appointment> pastAppointments;

    /*
     * Variables referring to the dialog used to see/delete an appointment.
     * If the dialog is not visible, then the variables are null.
     */
    private AlertDialog alertDialog;
    private Integer onItemClickPosition;
    private Long onItemClickId;

    // Collection of appointments.
    private SharedStore appointmentStore;


    /**
     * Hides to the user the placeholders used when no past appointments are available.
     * This is achieved by making invisible the placeholders defined in the layout.
     */
    private void hideNoAppointments() {
        ImageView placeholderImage = getView().findViewById(R.id.no_past_appointments_image);
        TextView placeholderText = getView().findViewById(R.id.no_past_appointments_text);

        placeholderImage.setVisibility(View.INVISIBLE);
        placeholderText.setVisibility(View.INVISIBLE);
    }

    /**
     * Shows to the user the placeholders used when no past appointments are available.
     * This is achieved by making visible the placeholders defined in the layout.
     */
    private void showNoAppointments() {
        ImageView placeholderImage = getView().findViewById(R.id.no_past_appointments_image);
        TextView placeholderText = getView().findViewById(R.id.no_past_appointments_text);

        placeholderImage.setVisibility(View.VISIBLE);
        placeholderText.setVisibility(View.VISIBLE);
    }

    /**
     * Deletes an appointment inside the list of past appointments.
     * @param position  the position of the appointment inside the pastAppointments list.
     */
    private void deleteAppointment(int position) {
        pastAppointments.remove(position);
        appointmentStore.saveValues(pastAppointments, CollectionType.PAST_APPOINTMENTS, getActivity(), Appointment.class);
    }

    /**
     * Creates a dialog to be shown to the user, assigning its reference
     * to the associated attribute.
     */
    private void createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ViewGroup viewGroup = getView().findViewById(android.R.id.content);

        builder.setView(LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_past_appointment, viewGroup, false));

        alertDialog = builder.create();
        alertDialog.setOnDismissListener(dialog -> releaseDialogVariables());
    }

    /**
     * Loads an appointment into a dialog, initializing correctly the relative fields.
     * @param appointment  the appointment to load.
     */
    private void loadAppointmentIntoDialog(Appointment appointment) {
        TextView description = alertDialog.findViewById(R.id.past_appointment_description);
        description.setText(String.format("%s\n\n%s", appointment.toString(), appointment.getOtherNotes()));
    }

    /**
     * Releases the variables referencing the dialog by setting to null
     * the associated attributes.
     */
    private void releaseDialogVariables() {
        alertDialog = null;
        onItemClickPosition = null;
        onItemClickId = null;
    }

    /**
     * Restores a dialog saved with onSaveInstanceState(), if the latter was
     * actually called and the dialog was visible.
     * @param savedInstanceState  the bundle containing the saved instance variables.
     */
    private void restorePreviousDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null || !savedInstanceState.getBoolean("dialogVisible"))
            return;

        onItemClick(
                pastAppointmentsListView,
                getView(),
                savedInstanceState.getInt("dialogOnItemClickPosition"),
                savedInstanceState.getLong("dialogOnItemClickId"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Get instance of the appointment store.
        appointmentStore = SharedStore.getInstance();

        /*
         * Add a listener to be executed when an appointment expires and is moved from
         * the next appointments collection to the past appointments one.
         * The listener is triggered by NextAppointmentsFragment.
         * The listener is used only to notify that the local "pastAppointments" list
         * must be updated; the bundle is actually ignored, since the data are exchanged
         * using the SharedPreference store.
         */
        getParentFragmentManager().setFragmentResultListener(
                "appointmentExpired",this, (requestKey, bundle) -> {
                    pastAppointments.clear();
                    pastAppointments.addAll(appointmentStore.getValues(
                            CollectionType.PAST_APPOINTMENTS, getActivity(), Appointment.class));
                });

        // Inflate the layout.
        return inflater.inflate(R.layout.fragment_past_appointments, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        pastAppointments = appointmentStore.getValues(CollectionType.PAST_APPOINTMENTS, getActivity(), Appointment.class);

        appointmentsAdapter = new ArrayAdapter<>(
                getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_1,
                pastAppointments);

        pastAppointmentsListView = view.findViewById(R.id.past_appointments_list);
        pastAppointmentsListView.setAdapter(appointmentsAdapter);

        // Add a listener to expand each item of the list.
        pastAppointmentsListView.setOnItemClickListener(this);

        /*
         * If onSaveInstanceState() has been called previously and
         * a dialog was visible, restore it and its information.
         */
        restorePreviousDialog(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Refresh the list every time the fragment is made visible.
        appointmentsAdapter.notifyDataSetChanged();

        if (pastAppointments.isEmpty())
            showNoAppointments();
        else
            hideNoAppointments();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Create a dialog to see, modify and delete an appointment.
        onItemClickPosition = position;
        onItemClickId = id;

        createDialog();
        alertDialog.show();
        loadAppointmentIntoDialog(pastAppointments.get(position));

        Button backButton = alertDialog.findViewById(R.id.past_appointment_back_button);
        ImageButton deleteButton = alertDialog.findViewById(R.id.past_appointment_delete_button);

        // Add a listener to delete the appointment.
        deleteButton.setOnClickListener((v) -> {
            deleteAppointment(position);

            Toast.makeText(getActivity(),
                    getActivity().getString(R.string.past_appointment_deletion_success_toast),
                    Toast.LENGTH_SHORT).show();

            alertDialog.dismiss();

            // Update the list of appointments shown to the user.
            onResume();
        });

        // Add a listener to close the dialog.
        backButton.setOnClickListener((v) -> alertDialog.dismiss());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (alertDialog != null) {
            outState.putBoolean("dialogVisible", true);
            outState.putInt("dialogOnItemClickPosition", onItemClickPosition);
            outState.putLong("dialogOnItemClickId", onItemClickId);
        } else
            outState.putBoolean("dialogVisible", false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (alertDialog != null)
            alertDialog.dismiss();
    }

}
