package it.unipi.catchamole.appointments;

import android.os.Bundle;

import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import it.unipi.catchamole.R;
import it.unipi.catchamole.utils.BaseActivity;


/**
 * Class representing the activity inside which the list of
 * next/past appointments can be managed.
 */
public class AppointmentsActivity extends BaseActivity {
    // Array containing the titles displayed in the TabLayout.
    private String[] tabTitles;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments);

        // Initialize the menu.
        createMenu();

        // Initialize the TabLayout titles.
        tabTitles = new String[] {
                getString(R.string.next_appointments),
                getString(R.string.past_appointments)
        };

        // Initialize the ViewPager.
        ViewPager2 viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(new AppointmentsPagerAdapter(this));

        // Initialize the TabLayout.
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        new TabLayoutMediator(tabLayout, viewPager, (tab, position)
                -> tab.setText(tabTitles[position])).attach();
    }
}
