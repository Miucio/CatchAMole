package it.unipi.catchamole.appointments.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;

import java.util.Calendar;
import java.util.Objects;

import it.unipi.catchamole.R;
import it.unipi.catchamole.appointments.Appointment;


/**
 * Class implementing the notification alarm manager used by NextAppointmentsFragment.
 * The manager is responsible for setting alarms associated to the appointments,
 * which are used in turn to fire notifications.
 * The class adopts the Singleton pattern, so that a single instance of
 * the relative object is instantiated and accessed at runtime.
 */
public class NotificationAlarmManager {
    // Single instance of the alarm manager (Singleton pattern).
    private final static NotificationAlarmManager singletonAlarmManager = new NotificationAlarmManager();

    // Intent action representing a notification alarm.
    private final String NOTIFICATION_ALARM_INTENT_ACTION =
            "it.unipi.catchamole.appointments.notification.NOTIFICATION_ALARM";


    /**
     * Private constructor used to implement the Singleton pattern.
     */
    private NotificationAlarmManager() {}

    /**
     * Generates a unique request code for a pending intent.
     * @param appointment  the appointment for which the alarm is being set.
     * @param firingTime   the firing time of the alarm.
     * @return             the unique request code.
     */
    private int generatePendingIntentRequestCode(Appointment appointment, long firingTime) {
        return appointment.hashCode() + Objects.hash(firingTime);
    }

    /**
     * Builds a pending intent used to set an alarm.
     * @param appointment  the appointment for which the alarm is being set.
     * @param titleExtra   the title that will be included in the notification
     *                     once the alarm is triggered.
     * @param firingTime   the firing time of the alarm.
     * @param context      the context of the caller.
     * @return             the requested pending intent.
     */
    private PendingIntent buildPendingIntent(Appointment appointment, String titleExtra,
                                             long firingTime, Context context) {
        Intent intent = new Intent(NOTIFICATION_ALARM_INTENT_ACTION);
        intent.setClass(context, NotificationAlarmReceiver.class);
        intent.putExtra("appointment", new Gson().toJson(appointment));
        intent.putExtra("title", titleExtra);

        return PendingIntent.getBroadcast(
                context,
                generatePendingIntentRequestCode(appointment, firingTime),
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * Provides the unique instance of the manager.
     * @return  the unique instance of the manager.
     */
    public static NotificationAlarmManager getInstance() {
        return singletonAlarmManager;
    }

    /**
     * Creates two alarms for an appointment: one triggered the day before the date and
     * another one triggered one hour before the date. An alarm is set only if
     * the corresponding firing time is not already expired.
     * @param appointment  the appointment for which the alarms must be set.
     * @param context      the context of the caller.
     */
    public void createNotificationAlarm(Appointment appointment, Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        long currentTime = Calendar.getInstance().getTimeInMillis();
        long dayBeforeFiringTime = appointment.getDate() - 24*60*60*1000;
        long hourBeforeFiringTime = appointment.getDate() - 60*60*1000;

        if (currentTime < dayBeforeFiringTime)
            alarmManager.setExactAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP,
                    dayBeforeFiringTime,
                    buildPendingIntent(
                            appointment,
                            context.getString(R.string.appointment_notification_title_day_before),
                            dayBeforeFiringTime,
                            context));

        if (currentTime < hourBeforeFiringTime)
            alarmManager.setExactAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP,
                    hourBeforeFiringTime,
                    buildPendingIntent(
                            appointment,
                            context.getString(R.string.appointment_notification_title_hour_before),
                            hourBeforeFiringTime,
                            context));
    }

    /**
     * Removes the two alarms set for the appointment. The alarms are removed without
     * checking if they were set, since this occurrence is handled by the AlarmManager.
     * @param appointment  the appointment for which the alarms must be removed.
     * @param context      the context of the caller.
     */
    public void removeNotificationAlarms(Appointment appointment, Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        long dayBeforeFiringTime = appointment.getDate() - 24*60*60*1000;
        long hourBeforeFiringTime = appointment.getDate() - 60*60*1000;

        alarmManager.cancel(
                buildPendingIntent(
                        appointment,
                        context.getString(R.string.appointment_notification_title_day_before),
                        dayBeforeFiringTime,
                        context));

        alarmManager.cancel(
                buildPendingIntent(
                        appointment,
                        context.getString(R.string.appointment_notification_title_hour_before),
                        hourBeforeFiringTime,
                        context));
    }
}
