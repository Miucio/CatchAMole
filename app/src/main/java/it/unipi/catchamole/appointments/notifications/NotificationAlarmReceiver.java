package it.unipi.catchamole.appointments.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import it.unipi.catchamole.R;
import it.unipi.catchamole.appointments.Appointment;
import it.unipi.catchamole.appointments.AppointmentsActivity;
import it.unipi.catchamole.store.SharedStore;
import it.unipi.catchamole.store.CollectionType;


/**
 * Class implementing the broadcast receiver used to intercept the intents fired:
 * 1) by the AlarmManager, when a notification for an appointment must
 *    be shown to the user;
 * 2) by the system, when the device completes the boot sequence, so that the
 *    alarms can be correctly restored (alarms set in the AlarmManager do not survive
 *    a reboot, they are not persistent).
 */
public class NotificationAlarmReceiver extends BroadcastReceiver {
    // Notification channel id and name.
    private final String CHANNEL_ID = "NextAppointments";
    private final String CHANNEL_NAME = "CatchAMole";

    // Intent action representing a notification alarm.
    private final String NOTIFICATION_ALARM_INTENT_ACTION =
            "it.unipi.catchamole.appointments.notification.NOTIFICATION_ALARM";

    // Intent action representing the end of the boot sequence of the device.
    private final String BOOT_COMPLETED_INTENT_ACTION = "android.intent.action.BOOT_COMPLETED";
    private final String QUICKBOOT_INTENT_ACTION = "android.intent.action.QUICKBOOT_POWERON";


    /**
     * Creates a notification channel through which the application can notify
     * the user about expiring appointments. This method can be called safely even if
     * the notification channel already exists: in the latter case, it performs no action.
     * @param context  the context in which the receiver is running.
     */
    private void createNotificationChannel(Context context) {
        String channelDescription = context.getString(R.string.notification_channel_description);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID, CHANNEL_NAME, android.app.NotificationManager.IMPORTANCE_HIGH);

            channel.setDescription(channelDescription);
            channel.enableLights(true);
            channel.setLightColor(Notification.DEFAULT_LIGHTS);
            channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), null);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{1000, 1000});

            android.app.NotificationManager manager = context.getSystemService(android.app.NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }

    /**
     * Creates and displays a notification for an appointment in the status bar.
     * @param context  the context in which the receiver is running.
     * @param intent   the received intent.
     */
    private void handleNotificationAlarmIntent(Context context, Intent intent) {
        createNotificationChannel(context);

        Appointment appointment;
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        try {
            appointment = new Gson().fromJson(
                    intent.getStringExtra("appointment"),
                    new TypeToken<Appointment>() {}.getType());
        } catch (JsonParseException e) {
            Log.e(this.getClass().getCanonicalName(), e.getMessage());
            return;
        }

        // Content of the notification.
        String contentTitle = intent.getStringExtra("title");
        String contentSmallText = appointment.toString();
        String contextBigText = appointment.getOtherNotes().equals("") ?
                appointment.toString() : appointment.toString() + "\n" + appointment.getOtherNotes();

        // Intent specifying the tap action of the notification.
        Intent notificationIntent = new Intent(context, AppointmentsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

        // Create the notification.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(contentTitle)
                .setContentText(contentSmallText)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(contextBigText))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        notificationManager.notify(0, builder.build());
    }

    /**
     * Restores the alarms previously set, when a reboot occurs.
     * @param context  the context in which the receiver is running.
     */
    private void handleBootAlarmIntent(Context context) {
        SharedStore appointmentStore = SharedStore.getInstance();
        NotificationAlarmManager notificationManager = NotificationAlarmManager.getInstance();

        List<Appointment> nextAppointments = appointmentStore.getValues(
                CollectionType.NEXT_APPOINTMENTS, context, Appointment.class);

        for (Appointment appointment : nextAppointments)
            notificationManager.createNotificationAlarm(appointment, context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(NOTIFICATION_ALARM_INTENT_ACTION))
            handleNotificationAlarmIntent(context, intent);
        else if (intent.getAction().equals(BOOT_COMPLETED_INTENT_ACTION) ||
                intent.getAction().equals(QUICKBOOT_INTENT_ACTION))
            handleBootAlarmIntent(context);
    }
}
