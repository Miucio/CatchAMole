package it.unipi.catchamole.appointments;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Objects;


/**
 * Class representing an appointment with a doctor.
 */
public class Appointment implements Comparable<Appointment> {
    private final String doctorName;
    private final String otherNotes;

    // Date of the appointment expressed in milliseconds.
    private final long date;


    /**
     * Constructor.
     * @param doctorName  the name of the doctor.
     * @param date        the date of the appointment expressed in milliseconds.
     * @param otherNotes  the notes associated with the appointment.
     */
    public Appointment(String doctorName, long date, String otherNotes) {
        this.doctorName = doctorName;
        this.date = date;
        this.otherNotes = otherNotes;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public long getDate() {
        return date;
    }

    public String getOtherNotes() {
        return otherNotes;
    }

    @Override
    public String toString() {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL, FormatStyle.SHORT);
        Instant instant = Instant.ofEpochMilli(date);
        ZonedDateTime dateTime = instant.atZone(ZoneId.systemDefault());

        return doctorName + "\n" + dateFormatter.format(dateTime);
    }

    @Override
    public int compareTo(Appointment o) {
        return Long.compare(date, o.date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Appointment that = (Appointment) o;
        return date == that.date &&
                doctorName.equals(that.doctorName) &&
                otherNotes.equals(that.otherNotes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(doctorName, date, otherNotes);
    }
}
