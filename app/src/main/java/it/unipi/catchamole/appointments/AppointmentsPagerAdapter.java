package it.unipi.catchamole.appointments;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;


/**
 * Class implementing the adapter used to manage the ViewPager
 * instantiated inside AppointmentsActivity.
 */
public class AppointmentsPagerAdapter extends FragmentStateAdapter {

    /**
     * Constructor.
     * @param fragmentActivity  the activity inside which the adapter is used.
     */
    public AppointmentsPagerAdapter(FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @Override
    public Fragment createFragment(int position) {
        if (position == 1)
            return new PastAppointmentsFragment();
        else
            return new NextAppointmentsFragment();
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
