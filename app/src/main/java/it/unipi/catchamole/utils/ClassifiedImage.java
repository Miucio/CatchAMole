package it.unipi.catchamole.utils;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import it.unipi.catchamole.R;

/**
 * Class representing an image classified by the MoleClassifier.
 */
public class ClassifiedImage {
    private final String path, folderName, fileName;
    private float melanomaProbability;
    private String date;
    private final BodyPartsType bodyPart;


    public ClassifiedImage(String path, String folderName, String fileName,
                           float melanomaProbability, BodyPartsType bodyPart) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        this.path = path;
        this.folderName = folderName;
        this.fileName = fileName;
        this.melanomaProbability = melanomaProbability;
        this.bodyPart = bodyPart;
        this.date = formatter.format(new Date());
    }

    public String getFileName() {
        return fileName;
    }

    public String getPath() {
        return path;
    }

    public String getFile() {
        return path + fileName;
    }

    public float getMelanomaProbability() {
        return melanomaProbability;
    }

    public void setMelanomaProbability(float melanomaProbability) {
        this.melanomaProbability = melanomaProbability;
    }

    public void setDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        this.date = formatter.format(date);
    }

    public String getDateTime() {
        return this.date;
    }

    public String toTranslatedString(Context context) {
        return date + "\n" + bodyPart.toTranslatedString(context) + "\n" +
                context.getString(R.string.melanoma_probability) + ": " +
                String.format(Locale.getDefault(), "%.4f%%", melanomaProbability*100);
    }

    public BodyPartsType getBodyPart() {
        return this.bodyPart;
    }
}
