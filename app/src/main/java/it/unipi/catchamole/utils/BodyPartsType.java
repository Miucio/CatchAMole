package it.unipi.catchamole.utils;

import android.content.Context;

import it.unipi.catchamole.R;

public enum BodyPartsType {
    LEFT_LEG,
    RIGHT_LEG,
    LEFT_ARM,
    RIGHT_ARM,
    HEAD,
    TORSO;

    public static BodyPartsType fromString(String s) {
        switch (s){
            case "LEFT_LEG" : return LEFT_LEG;
            case "RIGHT_LEG": return RIGHT_LEG;
            case "LEFT_ARM" : return LEFT_ARM;
            case "RIGHT_ARM": return RIGHT_ARM;
            case "HEAD"     : return HEAD;
            case "TORSO"    : return TORSO;
            default:
                throw new IllegalStateException("Unexpected value: " + s);
        }
    }

    public String toTranslatedString(Context context) {
        switch (name()) {
            case "LEFT_LEG" : return context.getString(R.string.body_part_left_leg);
            case "RIGHT_LEG": return context.getString(R.string.body_part_right_leg);
            case "LEFT_ARM" : return context.getString(R.string.body_part_left_arm);
            case "RIGHT_ARM": return context.getString(R.string.body_part_right_arm);
            case "HEAD"     : return context.getString(R.string.body_part_head);
            case "TORSO"    : return context.getString(R.string.body_part_torso);
            default         : return "";
        }
    }
}
