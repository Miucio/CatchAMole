package it.unipi.catchamole.utils;

import android.content.Context;
import android.graphics.Bitmap;
import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.common.ops.NormalizeOp;
import org.tensorflow.lite.support.image.ImageProcessor;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.image.ops.ResizeOp;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.IOException;

import it.unipi.catchamole.ml.Catchamole;


/**
 * Classifier used to detect if a mole in a picture is affected by malignant melanoma.
 */
public class MoleClassifier {
    // Parameters for image preprocessing.
    private int IMAGE_HEIGHT = 299;
    private int IMAGE_WIDTH = 299;
    private float NORMALIZE_MEAN = 0f;
    private float NORMALIZED_STD = 255f;

    // Classifier model.
    private Catchamole model;

    public MoleClassifier(Context context) throws IOException {
        model = Catchamole.newInstance(context);
    }

    /**
     * Preprocesses the image so that its size and pixels match the
     * input format expected by the model.
     * @param imageBitmap  the image to classify.
     * @return             the preprocessed image.
     */
    private TensorImage preprocessImage(Bitmap imageBitmap) {
        ImageProcessor imageProcessor =
                new ImageProcessor.Builder()
                        .add(new ResizeOp(IMAGE_HEIGHT, IMAGE_WIDTH, ResizeOp.ResizeMethod.BILINEAR))
                        .add(new NormalizeOp(NORMALIZE_MEAN, NORMALIZED_STD))
                        .build();

        TensorImage tensorImage = new TensorImage(DataType.FLOAT32);
        tensorImage.load(imageBitmap);
        tensorImage = imageProcessor.process(tensorImage);

        return tensorImage;
    }

    /**
     * Classifies an image determining the probability of being affected by malignant melanoma.
     * @param imageBitmap  the image to classify.
     * @return             the probability of being affected by malignant melanoma.
     */
    public float classify(Bitmap imageBitmap) {
        TensorImage tensorImage = preprocessImage(imageBitmap);

        // Create inputs for reference.
        TensorBuffer classifierInput = TensorBuffer.createFixedSize(
                new int[]{1, IMAGE_HEIGHT, IMAGE_WIDTH, 3}, DataType.FLOAT32);
        classifierInput.loadBuffer(tensorImage.getBuffer());

        // Run the model and get the result.
        Catchamole.Outputs outputs = model.process(classifierInput);
        TensorBuffer classifierOutput = outputs.getOutputFeature0AsTensorBuffer();

        return classifierOutput.getFloatArray()[0];
    }

    /**
     * Releases the resources used by the classifier to avoid leaks.
     * After calling this method, a new instance of the classifier must be created
     * in order to run a new classification task.
     */
    public void close() {
        model.close();
    }
}
