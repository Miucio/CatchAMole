package it.unipi.catchamole.utils;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import it.unipi.catchamole.R;
import it.unipi.catchamole.appointments.AppointmentsActivity;
import it.unipi.catchamole.classificationHistory.ClassificationHistoryActivity;
import it.unipi.catchamole.home.BodyActivity;
import it.unipi.catchamole.home.HomeActivity;


public class BaseActivity extends AppCompatActivity {
    protected Toolbar toolbar;
    protected DrawerLayout drawerLayout;
    protected NavigationView navigationView;


    private Intent createDestinationIntent(Class<?> destinationActivity) {
        if (destinationActivity.isInstance(this))
            return null;

        return new Intent(this, destinationActivity);
    }

    public Intent createMapsIntent(){
        String dermatologistSearchKeyword = getString(R.string.maps_dermatologist_search_keyword);
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + dermatologistSearchKeyword);

        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(getPackageManager()) != null)
            return mapIntent;

        Toast.makeText(this,
                getString(R.string.google_maps_not_installed_alert),
                Toast.LENGTH_LONG).show();

        return null;
    }

    private void addNavigationItems() {
        navigationView.setNavigationItemSelectedListener(item -> {
            Intent intent;

            switch (item.getItemId()) {
                case R.id.menu_new_scan:
                    intent = createDestinationIntent(BodyActivity.class);
                    break;
                case R.id.menu_scan_history:
                    intent = createDestinationIntent(ClassificationHistoryActivity.class);
                    break;
                case R.id.menu_nearby_dermatologists:
                    /*
                     * Not used anymore.
                     * createDestinationIntent(DermatologistMapsActivity.class);
                     */
                    intent = createMapsIntent();
                    break;
                case R.id.menu_appointments:
                    intent = createDestinationIntent(AppointmentsActivity.class);
                    break;
                default:
                    intent = createDestinationIntent(HomeActivity.class);
            }

            drawerLayout.close();

            if (intent != null)
                startActivity(intent);

            return true;
        });
    }

    public void createMenu() {
        toolbar = findViewById(R.id.side_navigation_toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigation_view);
        if(toolbar==null)
            Log.e("UNEXPECTEDNULLVALUE", "toolbar");

        if(drawerLayout==null)
            Log.e("UNEXPECTEDNULLVALUE", "drawerLayout");

        if(navigationView==null)
            Log.e("UNEXPECTEDNULLVALUE", "navigationView");

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this , drawerLayout, toolbar,
                R.string.open_menu_content_description,
                R.string.close_menu_content_description);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        addNavigationItems();

        // Avoid to show black icons by default.
        navigationView.setItemIconTintList(null);
    }
}
