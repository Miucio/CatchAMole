package it.unipi.catchamole.store;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;


/**
 * Class implementing the manager of the appointment store. The class adopts
 * the Singleton pattern, so that a single instance of the relative object
 * is instantiated and accessed at runtime.
 * The underlying SharedPreference object is thread safe.
 */
public class SharedStore {
    // Single instance of the store (Singleton pattern).
    private final static SharedStore singletonStore = new SharedStore();

    // Collection and key names used to store the appointments into the SharedPreferences.
    private final String APPOINTMENTS_COLLECTION = "APPOINTMENTS";
    private final String NEXT_APPOINTMENTS_KEY = "NextAppointments";
    private final String PAST_APPOINTMENTS_KEY = "PastAppointments";

    // Collection and key names used to store the classification results into the SharedPreferences.
    private final String CLASSIFICATIONS_COLLECTION = "CLASSIFICATIONS";
    private final String CLASSIFICATIONS_KEY = "Classifications";


    /**
     * Private constructor used to implement the Singleton pattern.
     */
    private SharedStore() {
    }

    /**
     * Provides the unique instance of the store.
     *
     * @return the unique instance of the store.
     */
    public static SharedStore getInstance() {
        return singletonStore;
    }

    /**
     * Retrieves the values stored in the specified the store.
     *
     * @param collectionType the type of collection from which the information must be retrieved.
     * @param context        the context of the caller.
     * @param classType      the class representing an element of the list.
     * @return the list of values.
     */
    public <T> List<T> getValues(CollectionType collectionType, Context context, Class<T> classType) {
        List<T> storedValues = new ArrayList<>();
        String defaultValue = "";
        String json;

        switch (collectionType) {
            case NEXT_APPOINTMENTS:
                json = context.getSharedPreferences(APPOINTMENTS_COLLECTION, Context.MODE_PRIVATE)
                        .getString(NEXT_APPOINTMENTS_KEY, defaultValue);
                break;
            case PAST_APPOINTMENTS:
                json = context.getSharedPreferences(APPOINTMENTS_COLLECTION, Context.MODE_PRIVATE)
                        .getString(PAST_APPOINTMENTS_KEY, defaultValue);
                break;
            case CLASSIFICATIONS:
                json = context.getSharedPreferences(CLASSIFICATIONS_COLLECTION, Context.MODE_PRIVATE)
                        .getString(CLASSIFICATIONS_KEY, defaultValue);
                break;
            default:
                json = defaultValue;
        }

        if (json.equals(defaultValue))
            return storedValues;

        try {
            storedValues = new Gson().fromJson(
                    json,
                    TypeToken.getParameterized(List.class, classType).getType());
        } catch (JsonParseException e) {
            System.out.println("Errore");
            Log.e(this.getClass().getCanonicalName(), e.getMessage());
        }

        return storedValues;
    }

    /**
     * Saves a list of values inside the specified store.
     *
     * @param values         the list of values to save.
     * @param collectionType the type of collection inside which the values must be stored.
     * @param classType      the class representing an element of the list.
     * @param context        the context of the caller.
     */
    public <T> void saveValues(List<T> values, CollectionType collectionType,
                               Context context, Class<T> classType) {
        switch (collectionType) {
            case NEXT_APPOINTMENTS:
                context.getSharedPreferences(APPOINTMENTS_COLLECTION, Context.MODE_PRIVATE)
                        .edit()
                        .putString(
                                NEXT_APPOINTMENTS_KEY,
                                new Gson().toJson(values, TypeToken.getParameterized(List.class, classType).getType()))
                        .apply();
                break;
            case PAST_APPOINTMENTS:
                context.getSharedPreferences(APPOINTMENTS_COLLECTION, Context.MODE_PRIVATE)
                        .edit()
                        .putString(
                                PAST_APPOINTMENTS_KEY,
                                new Gson().toJson(values, TypeToken.getParameterized(List.class, classType).getType()))
                        .apply();
                break;
            case CLASSIFICATIONS:
                context.getSharedPreferences(CLASSIFICATIONS_COLLECTION, Context.MODE_PRIVATE)
                        .edit()
                        .putString(
                                CLASSIFICATIONS_KEY,
                                new Gson().toJson(values, TypeToken.getParameterized(List.class, classType).getType()))
                        .apply();
                break;
        }
    }

    /**
     * Saves a single value inside the specified store.
     *
     * @param value          value to save.
     * @param collectionType the type of collection inside which the values must be stored.
     * @param classType      the class representing an element of the list.
     * @param context        the context of the caller.
     */
    public <T> void addValue(T value, CollectionType collectionType,
                             Context context, Class<T> classType) {
        List<T> tList = getValues(collectionType, context, classType);
        tList.add(value);

        switch (collectionType) {
            case NEXT_APPOINTMENTS:
                context.getSharedPreferences(APPOINTMENTS_COLLECTION, Context.MODE_PRIVATE)
                        .edit()
                        .putString(
                                NEXT_APPOINTMENTS_KEY,
                                new Gson().toJson(value, TypeToken.getParameterized(List.class, classType).getType()))
                        .apply();
                break;
            case PAST_APPOINTMENTS:
                context.getSharedPreferences(APPOINTMENTS_COLLECTION, Context.MODE_PRIVATE)
                        .edit()
                        .putString(
                                PAST_APPOINTMENTS_KEY,
                                new Gson().toJson(value, TypeToken.getParameterized(List.class, classType).getType()))
                        .apply();
                break;
            case CLASSIFICATIONS:
                context.getSharedPreferences(CLASSIFICATIONS_COLLECTION, Context.MODE_PRIVATE)
                        .edit()
                        .putString(
                                CLASSIFICATIONS_KEY,
                                new Gson().toJson(tList, TypeToken.getParameterized(List.class, classType).getType()))
                        .apply();
                break;
        }
    }
}
