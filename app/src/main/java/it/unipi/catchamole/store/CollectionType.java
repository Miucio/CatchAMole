package it.unipi.catchamole.store;


/**
 * Enumerator representing the type of store that must be used to save values.
 */
public enum CollectionType {
        NEXT_APPOINTMENTS,
        PAST_APPOINTMENTS,
        CLASSIFICATIONS
}
