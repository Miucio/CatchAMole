package it.unipi.catchamole.image;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import it.unipi.catchamole.R;
import it.unipi.catchamole.elaboratePhoto.ElaborateActivity;
import it.unipi.catchamole.home.BodyActivity;
import it.unipi.catchamole.utils.BodyPartsType;

/**
 * Activity to show image of the folder of the specific art
 */
public class ImageActivity extends AppCompatActivity implements itemClickListener {

    private static final String EXTRA_MESSAGE = "bodyPart";
    private static final String FOLDER_PATH = "folderPath";
    private static final String FOLDER_NAME = "folderName";
    private static final String IMAGE_NAME = "imageName";

    RecyclerView imageRecycler;
    ArrayList<pictureFacer> allPictures;
    ProgressBar load;
    String folderPath, folderName;
    TextView folderNameTextView;
    pictureBrowserFragment browser;

    private String setFolderNameTextView(BodyPartsType bodyPart) {
        switch (bodyPart) {
            case LEFT_LEG:
                return getString(R.string.body_part_left_leg);
            case RIGHT_LEG:
                return getString(R.string.body_part_right_leg);
            case LEFT_ARM:
                return getString(R.string.body_part_left_arm);
            case RIGHT_ARM:
                return getString(R.string.body_part_right_arm);
            case HEAD:
                return getString(R.string.body_part_head);
            case TORSO:
                return getString(R.string.body_part_torso);
            default:
                return "";
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        //Req. for write external storage permission
        if (checkCallingPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ImageActivity.this, new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 101);
        }

        folderName = getIntent().getStringExtra(EXTRA_MESSAGE);

        folderNameTextView = findViewById(R.id.foldername);
        folderNameTextView.setText(setFolderNameTextView(BodyPartsType.fromString(folderName)));

        folderPath = getExternalFilesDir("images" + "/" + folderName).getAbsolutePath() + "/";

        allPictures = new ArrayList<>();
        imageRecycler = findViewById(R.id.recycler);
        imageRecycler.addItemDecoration(new MarginDecoration(this));
        imageRecycler.hasFixedSize();
        load = findViewById(R.id.loader);

        FloatingActionButton cameraButton = findViewById(R.id.cameraButton);
        cameraButton.setOnClickListener(view -> {
            //Req. for camera runtime permission
            if (checkCallingPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ImageActivity.this, new String[]{
                        Manifest.permission.CAMERA
                }, 100);
            }
            dispatchTakePictureIntent();
        });


        if (allPictures.isEmpty()) {
            load.setVisibility(View.VISIBLE);
            allPictures = getAllImagesByFolder(folderPath);
            imageRecycler.setAdapter(new picture_Adapter(allPictures, ImageActivity.this, this));
            load.setVisibility(View.GONE);
        }
    }

    /**
     * @param holder   The ViewHolder for the clicked picture
     * @param position The position in the grid of the picture that was clicked
     * @param pics     An ArrayList of all the items in the Adapter
     */
    @Override
    public void onPicClicked(PicHolder holder, int position, ArrayList<pictureFacer> pics) {
        pictureBrowserFragment browser = pictureBrowserFragment.newInstance(pics, position, ImageActivity.this);
        getSupportFragmentManager()
                .beginTransaction()
                .addSharedElement(holder.picture, position + "picture")
                .add(R.id.displayContainer, browser)
                .addToBackStack(null)
                .commit();

    }

    /**
     * This Method gets all the images in the folder paths passed as a String to the method and returns
     * and ArrayList of pictureFacer a custom object that holds data of a given image
     *
     * @param path a String corresponding to a folder path on the device external storage
     */
    public ArrayList<pictureFacer> getAllImagesByFolder(String path) {
        ArrayList<pictureFacer> images = new ArrayList<>();

        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        if (files == null) {
            Log.d("Files", "Files is NULL");
            return null;
        }
        Log.d("Files", "Size: " + files.length);
        for (File file : files) {
            pictureFacer pic = new pictureFacer();
            pic.setPicturName(file.getName());
            pic.setPicturePath(path + "/" + file.getName());
            pic.setPictureSize(String.valueOf(file.length()));
            images.add(pic);
            Log.d("Files", "FileName:" + file.getName());
        }
        return images;
    }

    /**
     * Open the Elaborate activity
     *
     * @param name Name of the folder
     */
    public void openElaborateActivity(String name) {
        Intent intent = new Intent(this, ElaborateActivity.class);
        intent.putExtra(FOLDER_PATH, folderPath);
        intent.putExtra(FOLDER_NAME, folderName);
        intent.putExtra(IMAGE_NAME, name);
        intent.putExtra(EXTRA_MESSAGE, folderName);

        startActivity(intent);
    }

    /**
     * Get the picture and save
     */
    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) == null) {
            Toast.makeText(this,
                    getString(R.string.camera_not_installed_alert),
                    Toast.LENGTH_LONG).show();
            return;
        }
//      Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "$(applicationId}.provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 100);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 100) {
                allPictures = getAllImagesByFolder(folderPath);
                imageRecycler.setAdapter(new picture_Adapter(allPictures, ImageActivity.this, this));
            }
        }
    }

    String currentPhotoPath;

    /**
     * set path and name of photo
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir("images" + "/" + folderName);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, BodyActivity.class);

        // Remove all the activities higher in the stack.
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

}
