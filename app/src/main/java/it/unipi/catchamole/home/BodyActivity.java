package it.unipi.catchamole.home;

import android.content.Intent;
import android.os.Bundle;

import it.unipi.catchamole.R;
import it.unipi.catchamole.image.ImageActivity;
import it.unipi.catchamole.utils.BaseActivity;
import it.unipi.catchamole.utils.BodyPartsType;


public class BodyActivity extends BaseActivity {
    private static final String EXTRA_MESSAGE = "bodyPart";


    private void openImageActivity(BodyPartsType type) {
        Intent intent = new Intent(this, ImageActivity.class);
        intent.putExtra(EXTRA_MESSAGE, type.name());
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body);

        createMenu();

        // Left arm button
        findViewById(R.id.left_arm_button).setOnClickListener(v -> openImageActivity(BodyPartsType.LEFT_ARM));

        // Right arm button
        findViewById(R.id.right_arm_button).setOnClickListener(v -> openImageActivity(BodyPartsType.RIGHT_ARM));

        // Left leg button
        findViewById(R.id.left_leg_button).setOnClickListener(v -> openImageActivity(BodyPartsType.LEFT_LEG));

        // Right leg button
        findViewById(R.id.right_leg_button).setOnClickListener(v -> openImageActivity(BodyPartsType.RIGHT_LEG));

        // Head button
        findViewById(R.id.head_button).setOnClickListener(v -> openImageActivity(BodyPartsType.HEAD));

        // Torso button
        findViewById(R.id.torso_button).setOnClickListener(v -> openImageActivity(BodyPartsType.TORSO));
    }
}
