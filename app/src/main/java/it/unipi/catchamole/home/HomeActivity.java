package it.unipi.catchamole.home;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import it.unipi.catchamole.R;


/**
 * Home activity displaying the application logo.
 */
public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getBooleanExtra("exitApplication", false)) {
            finish();
        }

        setContentView(R.layout.activity_home);

        findViewById(R.id.home_button_enter).setOnClickListener(v -> {
            Intent intent = new Intent(this, BodyActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        /*
         * If the user taps the back button when she is in the home activity,
         * the application should be closed. To do this, it is necessary to
         * 1) clear the stack of activities;
         * 2) start the home activity again;
         * 3) finish the home activity, which is the only one in the stack.
         */
        Intent intent = new Intent(this, HomeActivity.class);

        // Remove all the activities higher in the stack.
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.putExtra("exitApplication", true);
        startActivity(intent);
    }
}
