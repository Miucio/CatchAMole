package it.unipi.catchamole.classificationHistory;

import android.content.Intent;
import android.os.Bundle;

import android.widget.ListView;

import java.util.List;

import it.unipi.catchamole.R;
import it.unipi.catchamole.store.CollectionType;
import it.unipi.catchamole.store.SharedStore;
import it.unipi.catchamole.utils.BaseActivity;
import it.unipi.catchamole.utils.ClassifiedImage;

public class ClassificationHistoryActivity extends BaseActivity {

    private ListView listView;
    private List<ClassifiedImage> classificationItems;

    private ClassificationArrayAdapter classificationArrayAdapter;
    private SharedStore collectionStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classification_history);
        collectionStore = SharedStore.getInstance();
        createMenu();

        listView = findViewById(R.id.classificationListView);

        classificationItems = collectionStore.getValues(
                CollectionType.CLASSIFICATIONS,
                this,
                ClassifiedImage.class);

        classificationArrayAdapter = new ClassificationArrayAdapter(this,classificationItems);

        listView.setAdapter(classificationArrayAdapter);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            starShowImageActivity(classificationItems.get(position), position);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        classificationItems.clear();
        classificationItems.addAll(collectionStore.getValues(
                CollectionType.CLASSIFICATIONS,
                this,
                ClassifiedImage.class));

        classificationArrayAdapter.notifyDataSetChanged();
    }

    private void starShowImageActivity(ClassifiedImage c, int pos) {
        Intent i = new Intent(this, ShowImageActivity.class);
        i.putExtra("classifiedImage_String", c.toTranslatedString(getApplicationContext()));
        i.putExtra("path", c.getPath());
        i.putExtra("file", c.getFile());
        i.putExtra("position", pos);
        startActivity(i);
    }
}