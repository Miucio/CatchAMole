package it.unipi.catchamole.classificationHistory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import it.unipi.catchamole.R;
import it.unipi.catchamole.store.CollectionType;
import it.unipi.catchamole.store.SharedStore;
import it.unipi.catchamole.utils.ClassifiedImage;

import java.util.ArrayList;
import java.util.List;

public class ClassificationArrayAdapter extends ArrayAdapter<ClassifiedImage> {

    Context context;

    public ClassificationArrayAdapter(Context context, List<ClassifiedImage> objects) {
        super(context, 0, objects);
        this.context=context;
    }

    private class ViewHolder {
        ImageView bodyPartIcon;
        TextView dateTextView;
        TextView melanomaProbabilityTextView;
        ImageView deleteIconImageView;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ClassifiedImage classifiedImage = getItem(position);
        ViewHolder holder ;

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.classification_item,
                                                parent, false);
            holder = new ViewHolder();
            holder.bodyPartIcon = convertView.findViewById(R.id.bodyPartImageView);
            holder.melanomaProbabilityTextView = convertView.findViewById(R.id.melanomaProbTxtView);
            holder.dateTextView = convertView.findViewById(R.id.dateTxtView);
            holder.deleteIconImageView = convertView.findViewById(R.id.deleteIconImageView);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.melanomaProbabilityTextView.setText(String.format("%.2f", classifiedImage.getMelanomaProbability()*100) +"%");
        holder.dateTextView.setText(classifiedImage.getDateTime());
        holder.bodyPartIcon.setImageBitmap(BitmapFactory.decodeFile(classifiedImage.getFile()));
        holder.deleteIconImageView.setOnClickListener(v -> {
                                                                deleteElaboration(position);
                                                                remove(classifiedImage);
                                                            }
                                                       );
        return convertView;

    }

    public void  deleteElaboration(int position){
        ArrayList<ClassifiedImage> clIm;
        clIm = (ArrayList<ClassifiedImage>) SharedStore.getInstance().getValues(
                CollectionType.CLASSIFICATIONS,
                getContext(),
                ClassifiedImage.class);

        clIm.remove(position);
        SharedStore.getInstance().saveValues(clIm, CollectionType.CLASSIFICATIONS, getContext(), ClassifiedImage.class);
    }

}