package it.unipi.catchamole.classificationHistory;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.util.List;

import it.unipi.catchamole.R;
import it.unipi.catchamole.store.CollectionType;
import it.unipi.catchamole.store.SharedStore;
import it.unipi.catchamole.utils.ClassifiedImage;


public class ShowImageActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);
        ImageView imageView = findViewById(R.id.imageView2);

        int position = getIntent().getIntExtra("position", 0);
        TextView t1 = findViewById(R.id.resultsElaborate);

        t1.setText(getIntent().getStringExtra("classifiedImage_String"));

        Button b = findViewById(R.id.cancelButton);
        b.setOnClickListener(v -> {
            deleteElaboration(position);
            finish();
        });

        String file = getIntent().getStringExtra("file");
        File image = new File(file);

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();

        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
        imageView.setImageBitmap(bitmap);
    }

    public void deleteElaboration(int position) {
        List<ClassifiedImage> clIm;

        clIm = SharedStore.getInstance().getValues(
                CollectionType.CLASSIFICATIONS,
                this,
                ClassifiedImage.class);

        clIm.remove(position);

        SharedStore.getInstance().saveValues(
                clIm,
                CollectionType.CLASSIFICATIONS,
                this,
                ClassifiedImage.class);
    }
}