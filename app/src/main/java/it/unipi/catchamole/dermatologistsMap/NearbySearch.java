package it.unipi.catchamole.dermatologistsMap;

import android.location.Location;
import com.google.maps.GeoApiContext;
import com.google.maps.PlacesApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.PlaceType;
import com.google.maps.model.PlacesSearchResponse;
import com.google.maps.model.RankBy;
import com.google.maps.model.LatLng;
import java.io.IOException;

public class NearbySearch {

    private LatLng location;

    public NearbySearch(Location l){
        this.location = new LatLng(l.getLatitude(),l.getLongitude());
    }

    public PlacesSearchResponse run(){
        PlacesSearchResponse placesSearchResponse = new PlacesSearchResponse();
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("google_maps_key")
                .build();

        try {
            placesSearchResponse = PlacesApi.nearbySearchQuery(context, this.location)
                    .radius(15000)
                    .rankby(RankBy.PROMINENCE)
                    .keyword("dermatologist")
                    .language("en")
                    .type(PlaceType.DOCTOR)
                    .await();
        } catch (ApiException | IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {

            return placesSearchResponse;
        }
    }

}
