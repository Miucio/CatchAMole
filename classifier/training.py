import tensorflow as tf
import tensorflow_hub as hub
import matplotlib.pyplot as plt
import pandas as pd
import os


tf.random.set_seed(7)
IMAGE_SIZE = [299, 299]  # Size expected by InceptionV3.
BATCH_SIZE = 64
OPTIMIZER = "rmsprop"
CLASS_NAMES = ["benign", "malignant"]  # 0 for benign, 1 for malignant.


def preprocess_image(filepath, label):
    # Load the raw data from the file as a string.
    img = tf.io.read_file(filepath)
    # Convert the compressed string to a 3D uint8 tensor.
    img = tf.image.decode_jpeg(img, channels=3)
    # Use "convert_image_dtype" to convert to floats in the [0,1] range.
    img = tf.image.convert_image_dtype(img, tf.float32)
    # Resize the image to the desired size.
    img = tf.image.resize(img, IMAGE_SIZE)

    return img, label


def prepare_for_training(ds, cache_filename, batch_size=64, shuffle_buffer_size=1000):
    ds.cache(cache_filename)
    ds = ds.shuffle(buffer_size=shuffle_buffer_size)  # Shuffle the dataset.
    ds = ds.repeat()  # Repeat forever.
    ds = ds.batch(batch_size)  # Split into batches.
    ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)  # Fetch batches in the background while training.

    return ds


def show_batch(batch):
    plt.figure(figsize=(12, 12))
    for n in range(25):
        ax = plt.subplot(5, 5, n + 1)
        plt.imshow(batch[0][n])
        plt.title(CLASS_NAMES[batch[1][n].numpy()].title())
        plt.axis("off")

    plt.show()


def main():
    # Load CSV files.
    df_train = pd.read_csv("csv/train.csv")
    df_valid = pd.read_csv("csv/valid.csv")

    n_training_samples = len(df_train)
    n_validation_samples = len(df_valid)
    print("Number of training samples:", n_training_samples)
    print("Number of validation samples:", n_validation_samples)

    # Prepare datasets.
    train_ds = tf.data.Dataset.from_tensor_slices((df_train["filepath"], df_train["label"]))
    valid_ds = tf.data.Dataset.from_tensor_slices((df_valid["filepath"], df_valid["label"]))

    valid_ds = valid_ds.map(preprocess_image)
    train_ds = train_ds.map(preprocess_image)

    valid_ds = prepare_for_training(valid_ds, batch_size=BATCH_SIZE, cache_filename="valid-cached-data")
    train_ds = prepare_for_training(train_ds, batch_size=BATCH_SIZE, cache_filename="train-cached-data")

    show_batch(next(iter(valid_ds)))  # Show one batch as an example.

    # Build the InceptionV3 model with pre-trained weights.
    # A new output layer with one unit is added (close to 0 means benign, close to 1 means malignant).
    inceptionV3_url = "https://tfhub.dev/google/tf2-preview/inception_v3/feature_vector/4"
    inceptionV3_model = tf.keras.Sequential([
        hub.KerasLayer(inceptionV3_url, output_shape=[2048], trainable=False),
        tf.keras.layers.Dense(1, activation="sigmoid")  # New output layer.
    ])

    inceptionV3_model.build([None, 299, 299, 3])
    inceptionV3_model.compile(loss="binary_crossentropy", optimizer=OPTIMIZER, metrics=["accuracy"])
    inceptionV3_model.summary()

    model_name = f"catch_a_mole_{BATCH_SIZE}_{OPTIMIZER}"
    tensorboard = tf.keras.callbacks.TensorBoard(log_dir=os.path.join("logs", model_name))

    # Save model checkpoint whenever we reach better weights.
    if not os.path.isdir("modelcheckpoints"):
        os.mkdir("modelcheckpoints")
    modelcheckpoint = tf.keras.callbacks.ModelCheckpoint("modelcheckpoints/" + model_name + "_{val_loss:.3f}.h5",
                                                         save_best_only=True, verbose=1)

    history = inceptionV3_model.fit(train_ds, validation_data=valid_ds,
                                    steps_per_epoch=n_training_samples // BATCH_SIZE,
                                    validation_steps=n_validation_samples // BATCH_SIZE, verbose=1, epochs=100,
                                    callbacks=[tensorboard, modelcheckpoint])


if __name__ == "__main__":
    main()
