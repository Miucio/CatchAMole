import os
import glob
import pandas as pd


# Generate CSV metadata file containing image paths and labels.
def generate_csv(folder, label2int):
    folder_name = os.path.basename(folder)
    labels = list(label2int)
    df = pd.DataFrame(columns=["filepath", "label"])

    i = 0
    for label in labels:
        print("Reading", os.path.join(folder, label, "*"))

        for filepath in glob.glob(os.path.join(folder, label, "*")):
            df.loc[i] = [filepath, label2int[label]]
            i += 1

    output_file = f"csv/{folder_name}.csv"
    print("Saving", output_file)
    df.to_csv(output_file)


if not os.path.isdir("csv"):
    os.mkdir("csv")

# Generate CSV files for all data portions, labeling nevus and
# seborrheic keratosis as 0 (benign), and melanoma as 1 (malignant).
generate_csv("data/train", {"nevus": 0, "seborrheic_keratosis": 0, "melanoma": 1})
generate_csv("data/valid", {"nevus": 0, "seborrheic_keratosis": 0, "melanoma": 1})
generate_csv("data/test", {"nevus": 0, "seborrheic_keratosis": 0, "melanoma": 1})
