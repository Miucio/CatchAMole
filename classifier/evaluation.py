import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import random
from sklearn.metrics import roc_curve, auc


tf.random.set_seed(7)
random.seed(7)
IMAGE_SIZE = [299, 299]  # Size expected by InceptionV3.


def preprocess_image(filepath, label):
    # Load the raw data from the file as a string.
    img = tf.io.read_file(filepath)
    # Convert the compressed string to a 3D uint8 tensor.
    img = tf.image.decode_jpeg(img, channels=3)
    # Use "convert_image_dtype" to convert to floats in the [0,1] range.
    img = tf.image.convert_image_dtype(img, tf.float32)
    # Resize the image to the desired size.
    img = tf.image.resize(img, IMAGE_SIZE)

    return img, label


def prepare_for_testing(ds, cache_filename, shuffle_buffer_size=1000):
    ds.cache(cache_filename)
    ds = ds.shuffle(buffer_size=shuffle_buffer_size)
    return ds


def convert_test_set_to_numpy(test_ds):
    n_testing_samples = len(test_ds)
    y_test = np.zeros((n_testing_samples,))
    x_test = np.zeros((n_testing_samples, 299, 299, 3))
    for i, (img, label) in enumerate(test_ds.take(n_testing_samples)):
        x_test[i] = img
        y_test[i] = label.numpy()

    return x_test, y_test


def plot_roc_curve(classifier, x_test, y_test):
    y_pred = classifier.predict(x_test)

    fpr, tpr, thresholds = roc_curve(y_test, y_pred, drop_intermediate=True)
    roc_auc = auc(fpr, tpr)

    for i in range(0, len(fpr)):
        print("Threshold: %s, TPR: %s, FPR: %s, TNR: %s, FNR: %s" % (thresholds[i], tpr[i],
                                                                     fpr[i], 1 - fpr[i], 1 - tpr[i]))

    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color='darkorange',
             lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc="lower right")
    plt.savefig("ROC.svg")


def main():
    df_test = pd.read_csv("csv/test.csv")
    print("Number of testing samples:", len(df_test))

    test_ds = tf.data.Dataset.from_tensor_slices((df_test["filepath"], df_test["label"]))
    test_ds = test_ds.map(preprocess_image)
    test_ds = prepare_for_testing(test_ds, cache_filename="test-cached-data")

    # Convert testing set to numpy array.
    x_test, y_test = convert_test_set_to_numpy(test_ds)

    # Load the model
    catchamole_classifier = tf.keras.models.load_model("model/catchamole")
    catchamole_classifier.summary()

    print("Evaluating the model...")
    loss, accuracy = catchamole_classifier.evaluate(x_test, y_test, verbose=0)
    print("Loss:", loss, "  Accuracy:", accuracy)

    plot_roc_curve(catchamole_classifier, x_test, y_test)


if __name__ == "__main__":
    main()
