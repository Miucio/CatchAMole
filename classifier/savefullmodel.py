import tensorflow as tf
import tensorflow_hub as hub
import shutil
import os

OPTIMIZER = "rmsprop"

# Load the InceptionV3 architecture and apply the best weights obtained with the training.
inceptionV3_url = "https://tfhub.dev/google/tf2-preview/inception_v3/feature_vector/4"
inceptionV3_model = tf.keras.Sequential([
    hub.KerasLayer(inceptionV3_url, output_shape=[2048], trainable=False),
    tf.keras.layers.Dense(1, activation="sigmoid")
])

inceptionV3_model.build([None, 299, 299, 3])
inceptionV3_model.compile(loss="binary_crossentropy", optimizer=OPTIMIZER, metrics=["accuracy"])
inceptionV3_model.load_weights("modelcheckpoints/catch_a_mole_64_rmsprop_0.364.h5")

# Clear the model directory.
if os.path.isdir("model"):
    shutil.rmtree("model")
os.mkdir("model")

# Save the entire model as a SavedModel.
inceptionV3_model.save("model/catchamole")

# Save an addition copy of the model converting from SavedModel to TensorFlow Lite.
converter = tf.lite.TFLiteConverter.from_saved_model("model/catchamole")
tflite_model = converter.convert()

with open("model/catchamole.tflite", "wb") as f:
    f.write(tflite_model)
