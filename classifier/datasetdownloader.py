from tensorflow.keras.utils import get_file
import os
import zipfile

# Dataset from https://github.com/udacity/dermatologist-ai
training_url = "https://s3-us-west-1.amazonaws.com/udacity-dlnfd/datasets/skin-cancer/train.zip"  # 5.3GB
validation_url = "https://s3-us-west-1.amazonaws.com/udacity-dlnfd/datasets/skin-cancer/valid.zip"  # 824.5MB
test_url = "https://s3-us-west-1.amazonaws.com/udacity-dlnfd/datasets/skin-cancer/test.zip"  # 5.1GB

for i, download_link in enumerate([validation_url, training_url, test_url]):
    temp_file = f"temp{i}.zip"
    data_dir = get_file(origin=download_link, fname=os.path.join(os.getcwd(), temp_file))

    print("Extracting", download_link)

    with zipfile.ZipFile(data_dir, "r") as z:
        z.extractall("data")

    os.remove(temp_file)
