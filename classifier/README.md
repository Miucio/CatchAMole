# CatchAMole classifier
The steps to reproduce the entire classification process are:
1. run _datasetdownloader.py_ to download the training, test and validation sets (~10GB);
2. run _csvgenerator.py_ to prepare the needed metadata for tranining and evaluation;
3. run _training.py_ to train the classifier. Multiple model checkpoints are saved in the _modelcheckpoints_
   folder with the format _catch_a_mole_64_rmsprop_X_;
4. identify the checkpoint with the lowest value of _X_ (best model). Run _savefullmodel.py_ changing the
   name of the loaded checkpoint with the one you identified before (the name is hardcoded at line 17); 
5. run _evaluation.py_ to obtain the score of the model with different thresholds and the ROC curve.